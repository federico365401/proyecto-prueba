console.log('Hola, Mundo')

const http = require('http')
const host = '127.0.0.1'
const puerto = 8000
const server = http.createServer((req, res) => {

    res.statusCode = 200
    res.setHeader('Content-Type', 'text/plain')
    res.end('Hola Mundo ')

}) 

server.listen(puerto, host, () => {
    console.log('Servidor activo en http://${host}:${puerto}/')
})